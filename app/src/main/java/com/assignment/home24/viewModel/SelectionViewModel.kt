package com.assignment.home24.viewModel

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.view.View
import com.assignment.home24.data.Article
import com.assignment.home24.data.source.ArticlesRepository
import com.assignment.home24.view.review.ReviewActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SelectionViewModel @Inject constructor(repository: ArticlesRepository) : BaseViewModel() {

    private val articles by lazy { MutableLiveData<ArrayList<Article>>() }

    private val disposable by lazy { CompositeDisposable() }

    val reviewCount by lazy { ObservableField<Int>().apply { set(0) } }

    val likeCount by lazy { ObservableField<Int>().apply { set(0) } }

    val totalCount by lazy { ObservableField<Int>().apply { set(0) } }

    val isLoading by lazy { ObservableField<Boolean>() }

    val endOfPage by lazy { ObservableField<Boolean>() }

    init {
        disposable.add(repository.getArticles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoading.set(true) }
                .doAfterTerminate { isLoading.set(false) }
                .subscribe({
                    articles.value = it.embedded.articles
                    totalCount.set(it.embedded.articles.size)
                }, {
                    it.printStackTrace()
                }))
    }

    fun getArticlesData() = articles

    fun incrementReviewCount(incLikeCount: Boolean) {
        if (reviewCount.get() == totalCount.get()) {
            reviewCount.set(totalCount.get())
        } else {
            reviewCount.set(reviewCount.get()?.inc())
            if (incLikeCount)
                likeCount.set(likeCount.get()?.inc())
        }
        endOfPage.set(reviewCount.get() == totalCount.get())
    }

    fun decrementReviewCount(decLikeCount: Boolean) {
        endOfPage.set(false)
        if (reviewCount.get() == 0) {
            reviewCount.set(0)
            likeCount.set(0)
        } else {
            reviewCount.set(reviewCount.get()?.dec())
            if (decLikeCount)
                likeCount.set(likeCount.get()?.dec())
        }
    }

    fun onReviewButtonClick(view: View) {
        ReviewActivity.launchActivity(view.context, articles.value)
    }

    override fun onCleared() {
        disposable.clear()
        disposable.dispose()
        super.onCleared()
    }
}