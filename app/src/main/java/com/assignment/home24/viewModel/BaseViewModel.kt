package com.assignment.home24.viewModel

import android.arch.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()