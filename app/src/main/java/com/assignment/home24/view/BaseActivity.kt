package com.assignment.home24.view

import android.arch.lifecycle.ViewModelProvider
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.assignment.home24.di.Injectable
import com.assignment.home24.viewModel.BaseViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mViewDataBinding: T
    lateinit var mViewModel: V

    //Abstract functions
    abstract fun getBindingVariable(): Int

    abstract fun getViewModel(): V
    abstract fun getLayoutResourceId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (this is Injectable) {
            AndroidInjection.inject(this)
        }

        performDataBinding()
    }

    private fun performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutResourceId())

        this.mViewModel = getViewModel()

        mViewDataBinding.apply {
            setVariable(getBindingVariable(), mViewModel)
            executePendingBindings()
        }
    }

    /**
     * Return the associated View with binding
     */
    fun getViewDataBinding(): T {
        return mViewDataBinding
    }
}