package com.assignment.home24.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.assignment.home24.R
import com.assignment.home24.view.selection.SelectionActivity
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        btn_start.setOnClickListener {
            startActivity(Intent(this@StartActivity, SelectionActivity::class.java))
        }
    }
}
