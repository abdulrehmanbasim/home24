package com.assignment.home24.view.selection

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.android.databinding.library.baseAdapters.BR
import com.assignment.home24.R
import com.assignment.home24.data.Article
import com.assignment.home24.databinding.ActivitySelectionBinding
import com.assignment.home24.di.Injectable
import com.assignment.home24.view.BaseActivity
import com.assignment.home24.viewModel.SelectionViewModel
import com.yarolegovich.discretescrollview.DSVOrientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_selection.*

class SelectionActivity : BaseActivity<ActivitySelectionBinding, SelectionViewModel>(), Injectable {
    override fun getViewModel() = ViewModelProviders.of(this, viewModelFactory).get(SelectionViewModel::class.java)

    override fun getBindingVariable() = BR.selectionViewModel

    override fun getLayoutResourceId() = R.layout.activity_selection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDsScrollView()
        observeViewModel()
    }

    private fun setupDsScrollView() {
        dsScrollView.apply {
            setOrientation(DSVOrientation.HORIZONTAL)
            adapter = ArticleAdapter().apply {
                setOnItemActionListener(object : ArticleAdapter.OnItemActionListener() {
                    override fun onActionPerformed(action: Action, obj: Any) {
                        when (action) {
                            Action.LIKE -> mViewModel.incrementReviewCount(true)
                            Action.DISLIKE -> mViewModel.incrementReviewCount(false)
                            Action.UNDO_LIKE -> mViewModel.decrementReviewCount(true)
                            Action.UNDO_DISLIKE -> mViewModel.decrementReviewCount(false)
                        }

                        if (obj is Article && obj.position < itemCount - 1) {
                            smoothScrollToPosition(obj.position + 1)
                        }
                    }
                })
            }
            setItemTransitionTimeMillis(100)
            setItemTransformer(ScaleTransformer.Builder().setMinScale(0.9f).build())
        }
    }

    private fun observeViewModel() {
        mViewModel.getArticlesData().observe(this, Observer {
            it?.let { items ->
                setupData(items)
            }
        })
    }

    private fun setupData(items: ArrayList<Article>) {
        (dsScrollView.adapter as ArticleAdapter).apply {
            addAll(items)
        }
    }
}
