package com.assignment.home24.view

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import com.assignment.home24.R
import com.assignment.home24.util.bounce

class StateImageView(context: Context, attributeSet: AttributeSet) : ImageView(context, attributeSet) {

    var currentState = State.DISABLED
    set(value) {
        field = value
        setImageResource(when (currentState) {
            State.ENABLED -> stateEnabledResource
            State.DISABLED -> stateDisabledResource
        })
    }

    var stateEnabledResource: Int = 0
    var stateDisabledResource: Int = 0

    init {
        val typedArray = context.theme.obtainStyledAttributes(attributeSet,
                R.styleable.StateImageView,
                0, 0)
        try {
            stateEnabledResource = typedArray.getResourceId(R.styleable.StateImageView_enabledResource, 0)
            stateDisabledResource = typedArray.getResourceId(R.styleable.StateImageView_disabledResource, 0)
            currentState = State.values()[typedArray.getInt(R.styleable.StateImageView_state, 0)]
        } catch (ex: Exception) {
            ex.printStackTrace()
        } finally {
            typedArray.recycle()
        }
    }

    fun toggleState(listener: Animator.AnimatorListener) {
        setImageResource(when (currentState) {
            State.ENABLED -> {
                currentState = State.DISABLED
                stateDisabledResource
            }
            State.DISABLED -> {
                currentState = State.ENABLED
                stateEnabledResource
            }
        })

        bounce(listener)
    }
}

enum class State constructor(val value: Int) { DISABLED(0), ENABLED(1) }