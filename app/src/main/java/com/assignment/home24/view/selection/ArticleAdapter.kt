package com.assignment.home24.view.selection

import android.animation.Animator
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.assignment.home24.R
import com.assignment.home24.data.Article
import com.assignment.home24.databinding.ItemArticleBinding
import com.assignment.home24.util.bounce
import com.assignment.home24.view.StateImageView

/*
* Adapter Extended from recycler view adapter
 */
class ArticleAdapter : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    private val items = ArrayList<Article>()
    private lateinit var binding: ItemArticleBinding

    private var onItemActionListener: OnItemActionListener? = null

    /*
    * set item view and binds click listener in xml
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_article, parent, false)
        binding.clickListener = onItemActionListener
        return ViewHolder(binding)
    }

    /*
    * set item click callbacks
     */
    fun setOnItemActionListener(onItemClickListener: OnItemActionListener) {
        this.onItemActionListener = onItemClickListener
    }

    /*
    * adding list of vehicles, will clear adapter if given list is null
     */
    fun addAll(article: ArrayList<Article>?) {
        items.clear()
        article?.let { items.addAll(article) }
        notifyDataSetChanged()
    }

    /*
    * total count of items in adapter
     */
    override fun getItemCount(): Int {
        return items.size
    }

    /*
    * @return item at specific position in adapter
     */
    fun getItem(position: Int) = items[position]

    /*
    * binds data on @param position to view
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    /*
    * View holder for holding item views
     */
    class ViewHolder(private val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(article: Article) {
            article.position = adapterPosition
            binding.article = article
        }
    }

    /*
    * Callback for click on item in an adapter
     */
    abstract class OnItemActionListener {

        fun onItemClick(view: View, obj: Any) {
            val item = obj as Article
            var toggleState = false
            var action: Action? = null

            when (view.id) {
                R.id.likeBtn -> {
                    if (!item.disLiked) {
                        toggleState = true
                        item.isLiked = !item.isLiked
                        action = if (item.isLiked) Action.LIKE else Action.UNDO_LIKE
                    }
                }
                R.id.dislikeBtn -> {
                    if (!item.isLiked) {
                        toggleState = true
                        item.disLiked = !item.disLiked
                        action = if (item.disLiked) Action.DISLIKE else Action.UNDO_DISLIKE
                    }
                }
            }

            if (toggleState) {
                (view as? StateImageView)?.toggleState(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {
                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        action?.let {
                            onActionPerformed(action, obj)
                        }
                    }

                    override fun onAnimationCancel(p0: Animator?) {
                    }

                    override fun onAnimationStart(p0: Animator?) {
                    }

                })
            }
        }

        abstract fun onActionPerformed(action: Action, obj: Any)
    }

}

enum class Action { LIKE, DISLIKE, UNDO_LIKE, UNDO_DISLIKE }

