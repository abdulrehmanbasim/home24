package com.assignment.home24.view.review

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.widget.Toast
import com.assignment.home24.R
import com.assignment.home24.data.Article
import kotlinx.android.synthetic.main.activity_review.*

class ReviewActivity : AppCompatActivity() {

    private lateinit var _articles: ArrayList<Article>

    private val gridLayoutManager: GridLayoutManager by lazy { GridLayoutManager(this, 1) }

    companion object {

        private const val KEY_ARTICLES_LIST = "articlesList"

        fun launchActivity(source: Context, articles: ArrayList<Article>?) {
            val intent = Intent(source, ReviewActivity::class.java).apply {
                putParcelableArrayListExtra(KEY_ARTICLES_LIST, articles)
            }
            source.startActivity(intent)
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.action_grid -> {
                changeView(2)
                return@OnNavigationItemSelectedListener true
            }
            R.id.action_list -> {
                changeView(1)

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_review)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        intent?.let {
            _articles = it.getParcelableArrayListExtra(KEY_ARTICLES_LIST)
            showData()

        } ?: kotlin.run {
            Toast.makeText(this, "Empty data", Toast.LENGTH_SHORT).show()
        }
    }

    private fun changeView(spanCount: Int) {
        if(gridLayoutManager.spanCount == spanCount) return

        gridLayoutManager.spanCount = spanCount

        rv_articles.adapter?.apply {
            (this as ReviewAdapter).toggleItemViewType()
        }
    }

    private fun showData() {
        rv_articles.apply {
            layoutManager = gridLayoutManager
            adapter = ReviewAdapter(_articles)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
        }
    }
}