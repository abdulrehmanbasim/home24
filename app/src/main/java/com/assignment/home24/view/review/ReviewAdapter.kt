package com.assignment.home24.view.review

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.assignment.home24.R
import com.assignment.home24.data.Article
import com.assignment.home24.databinding.ItemArticleGridBinding
import com.assignment.home24.databinding.ItemArticleListBinding


private const val LIST_ITEM = 0
private const val GRID_ITEM = 1

class ReviewAdapter(private val dataList: List<Article>) : RecyclerView.Adapter<BaseViewHolder>() {

    private var isGridView = false

    fun toggleItemViewType() {
        isGridView = !isGridView

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == LIST_ITEM) {
            val binding = DataBindingUtil.inflate<ItemArticleListBinding>(LayoutInflater.from(parent.context), R.layout.item_article_list, parent, false)
            ListViewHolder(binding)

        } else {
            val binding = DataBindingUtil.inflate<ItemArticleGridBinding>(LayoutInflater.from(parent.context), R.layout.item_article_grid, parent, false)
            GridViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bindData(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isGridView) {
            GRID_ITEM
        } else {
            LIST_ITEM
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ListViewHolder(private var binding: ViewDataBinding) : BaseViewHolder(binding) {
        override fun bindData(position: Int) {
            (binding as ItemArticleListBinding).article = dataList[position]
        }
    }

    inner class GridViewHolder(private var binding: ViewDataBinding) : BaseViewHolder(binding) {
        override fun bindData(position: Int) {
            (binding as ItemArticleGridBinding).article = dataList[position]
        }
    }
}

abstract class BaseViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bindData(position: Int)
}
