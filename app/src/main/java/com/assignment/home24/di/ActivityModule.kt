package com.assignment.home24.di

import com.assignment.home24.view.selection.SelectionActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector()
    internal abstract fun contributeSelectionActivity(): SelectionActivity
}