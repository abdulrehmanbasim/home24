package com.assignment.home24.data.source.remote

import com.assignment.home24.data.ApiResponse
import io.reactivex.Single
import retrofit2.http.GET

const val ARTICLES = "v2.0/categories/100/articles?appDomain=1&locale=de_DE&limit=10"
interface RestApi {
    @GET(ARTICLES)
    fun getArticles(): Single<ApiResponse>
}