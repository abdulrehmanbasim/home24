package com.assignment.home24.data

/*
* Wrapper class for response for parsing it in to proper states
* */

class Resource<T>(val status: Status, val data: T?, val error: Error?, val message: String?) {

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null, null)
        }

        fun <T> error(msg: Error): Resource<T> {
            return Resource(Status.ERROR, null, msg, null)
        }

        fun <T> loading(message: String?): Resource<T> {
            return Resource(Status.LOADING, null, null, message)
        }
    }
}

enum class Status {
    LOADING, SUCCESS, ERROR
}