package com.assignment.home24.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.text.FieldPosition

data class ApiResponse(
        @SerializedName("articlesCount") val articlesCount: Int,
        @SerializedName("_embedded") val embedded: Embedded
)

data class Embedded(
        @SerializedName("articles") val articles: ArrayList<Article>
)

@Parcelize
data class Article(
        @SerializedName("sku") val sku: String?,
        @SerializedName("title") val title: String?,
        @SerializedName("media") var media: List<Media>?,
        @SerializedName("_metadata") val metadata: Metadata?,
        var isLiked: Boolean = false,
        var disLiked: Boolean = false,
        var position: Int
) : Parcelable


@Parcelize
data class Media(
        @SerializedName("uri") val uri: String,
        @SerializedName("mimeType") val mimeType: String,
        @SerializedName("type") val type: String?,
        @SerializedName("priority") val priority: Int,
        @SerializedName("size") val size: String?
) : Parcelable

@Parcelize
data class Metadata(
        @SerializedName("type") val type: String
) : Parcelable