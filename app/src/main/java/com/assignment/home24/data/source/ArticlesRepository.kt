package com.assignment.home24.data.source

import com.assignment.home24.data.source.remote.RestApi
import javax.inject.Inject

class ArticlesRepository @Inject constructor(private val networkService: RestApi) {
    fun getArticles() = networkService.getArticles()
}