package com.assignment.home24.util

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.ImageView

fun View.bounce(listener: Animator.AnimatorListener) {
    val overshootInterpolator = OvershootInterpolator(4f)

    val bounceAnimX = ObjectAnimator.ofFloat(this, "scaleX", 0.2f, 1f)
    bounceAnimX.duration = 300
    bounceAnimX.interpolator = overshootInterpolator

    val bounceAnimY = ObjectAnimator.ofFloat(this, "scaleY", 0.2f, 1f)
    bounceAnimY.duration = 300
    bounceAnimY.interpolator = overshootInterpolator

    val animatorSet = AnimatorSet()
    animatorSet.play(bounceAnimX).with(bounceAnimY)
    animatorSet.addListener(listener)
    animatorSet.start()
}
