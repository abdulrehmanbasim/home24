package com.assignment.home24.util

import android.annotation.SuppressLint
import android.databinding.BindingAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.assignment.home24.data.Media
import com.assignment.home24.view.State
import com.assignment.home24.view.StateImageView
import com.assignment.home24.view.selection.Action
import com.bumptech.glide.Glide


@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, media: List<Media>?) {
    media?.isNotEmpty().let {
        Glide.with(view.context).load(media?.get(0)?.uri).into(view)
    }
}

@BindingAdapter("imageState")
fun setImageState(view: StateImageView, status: Boolean) {
    view.currentState = if (status) State.ENABLED else State.DISABLED
}

@SuppressLint("SetTextI18n")
@BindingAdapter("reviewCount", "total")
fun setText(textView: TextView, reviewCount: Int, total: Int) {
    textView.text = "$reviewCount/$total"
}
