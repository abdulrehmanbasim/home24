# Home24-Codding Challange

The application will use Home24 public API to load several articles from remote server and
present them to the user one by one. User should mark the article as “liked” or ”disliked”
before another article will be presented. User will have a possibility to review his choices by
pressing the relevant button.
This app implements MVVM architecture using Dagger2 and Retrofit for network calls.

# Main Packages
  - data: It contains all the data accessing and manipulating components.
  - di: Dependency providing classes using Dagger2.
  - util: Utility classes.
  - view: View classes.
  - viewModel: View models for view classes

Classes have been designed in such a way that it could be inherited and maximize the code reuse.

# Development Envoirnment
  - Android Studio version 3.1.4
  - Kotlin version 1.2.61
  - Minimum Sdk Version 14

# Dependencies in graddle
Main dependencies added in build.graddle are:
  - Support Libraries version 27.1.1
  - Retrofit version 2.4.0, used for network calls
  - Dagger version 2.15, used for dependency injection
  - Android LifeCycle Extensions version 1.1.1, for MVVM
  - Discrete Scrollview version 1.4.9
  - RxJava version 2.1.13
  - RxAndroid version 2.0.2

# Instructions
You can open this project in Android Studio using the following steps:
  - Click File > New > Import Project.
  - Select this project directory, and click OK. The project will run in Android Studio.
> Note: All images are downloaded from google and used
<https://romannurik.github.io/AndroidAssetStudio/>
for creating images formultiple screen sizes

# Developed By
  - Abdul Rehman Basim
  - <abdulrehmanbasim@gmail.com>